package ee.bcs.valiit.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class Meeting {

    /*Omadused*/

    private int meetingId;
    private int meetingOwnerId;
    private int meetingTypeId;
    private String subject;
    private String dateTimeForMeeting;
    private String detailsOfMeeting;
    private User meetingHolder;
    private String uniqueHash = UUID.randomUUID().toString();

    /*Builderid*/

    public Meeting() {
    }

    public Meeting(ResultSet result) {
        try {
            this.setMeetingId(result.getInt("id"));
            this.setMeetingOwnerId(result.getInt("meeting_owner_id"));
            this.setMeetingTypeId(result.getInt("meeting_type_id"));
            this.setSubject(result.getString("subject"));
            this.setDateTimeForMeeting(result.getString("datetime"));
            this.setDetailsOfMeeting(result.getString("details_of_meeting"));
            if (result.getString("meeting_uuid").equals("")) {
                this.setUniqueHash(UUID.randomUUID().toString());
            } else {
                this.setUniqueHash(result.getString("meeting_uuid"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public Meeting(int meetingOwnerId, int meetingTypeId, String subject, String dateTimeForMeeting, String detailsOfMeeting) {
        this.meetingOwnerId = meetingOwnerId;
        this.meetingTypeId = meetingTypeId;
        this.subject = subject;
        this.dateTimeForMeeting = dateTimeForMeeting;
        this.detailsOfMeeting = detailsOfMeeting;
    }

    /*Getterid, setterid*/

    public String getUniqueHash() {
        return uniqueHash;
    }

    public void setUniqueHash(String uniqueHash) {
        this.uniqueHash = uniqueHash;
    }

    public User getMeetingHolder() {
        return meetingHolder;
    }

    public void setMeetingHolder(User meetingHolder) {
        this.meetingHolder = meetingHolder;
    }

    public int getMeetingId() {
        return meetingId;
    }

    public void setMeetingId(int meetingId) {
        this.meetingId = meetingId;
    }

    public int getMeetingOwnerId() {
        return meetingOwnerId;
    }

    public void setMeetingOwnerId(int meetingOwnerId) {
        this.meetingOwnerId = meetingOwnerId;
    }

    public int getMeetingTypeId() {
        return meetingTypeId;
    }

    public void setMeetingTypeId(int meetingTypeId) {
        this.meetingTypeId = meetingTypeId;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDateTimeForMeeting() {
        return dateTimeForMeeting;
    }

    public void setDateTimeForMeeting(String dateTimeForMeeting) {
        this.dateTimeForMeeting = dateTimeForMeeting;
    }

    public String getDetailsOfMeeting() {
        return detailsOfMeeting;
    }

    public void setDetailsOfMeeting(String detailsOfMeeting) {
        this.detailsOfMeeting = detailsOfMeeting;
    }


}

