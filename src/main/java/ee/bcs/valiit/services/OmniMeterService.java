package ee.bcs.valiit.services;

import ee.bcs.valiit.model.Feedbackform;
import ee.bcs.valiit.model.Meeting;
import ee.bcs.valiit.model.User;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.sql.*;

public class OmniMeterService {

    public static final String SQL_CONNECTION_URL = "jdbc:mysql://localhost:3306/mydb";
    public static final String SQL_USERNAME = "root";
    public static final String SQL_PASSWORD = "tere";

    public static ResultSet executeSql(String sql) {
        try {
            Class.forName("org.mariadb.jdbc.Driver");
            try (Connection conn = DriverManager.getConnection(SQL_CONNECTION_URL, SQL_USERNAME, SQL_PASSWORD)) {
                try (Statement stmt = conn.createStatement()) {
                    return stmt.executeQuery(sql);
                }
            }
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
            return null;
        }
    }


    /*USER Under this comment there is all for user*/

    public static User getAuthenticatedUser(String email, String password) {
        String sql = String.format("SELECT * FROM user WHERE email = '%s' AND password = '%s'", email, password);
        ResultSet userRecord = executeSql(sql);
        try {
            if(userRecord != null && userRecord.next()){
                return instantiateUser(userRecord);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected static User instantiateUser(ResultSet userRecord) throws SQLException {
        User user = new User();
        user.setId(userRecord.getInt("id"));
        user.setFirstName(userRecord.getString("first_name"));
        user.setLastName(userRecord.getString("last_name"));
        user.setDepartment(userRecord.getString("department"));
        user.setEmail(userRecord.getString("email"));
        user.setPermissionId(userRecord.getInt("role"));
        return user;
    }


    public static List<User> getUsers() {
        List<User> users = new ArrayList<User>();
        try {
            ResultSet result = executeSql("select * from user");
            if (result != null) {
                while (result.next()) {
                    users.add(new User(result));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return users;
    }

    public static boolean userDoesntExsist(String email) {
        return getUserByEmail(email) == null;
    }


    public static User getUserByEmail(String email) {
        try {
            ResultSet result = executeSql("select id, first_name, last_name, password, role, department, email from user where email = '" + email + "'");
            if (result != null) {
                if (result.next()) {
                    User user = new User();
                    user.setId(result.getInt("id"));
                    user.setFirstName(result.getString("first_name"));
                    user.setLastName(result.getString("last_name"));
                    user.setPermissionId(result.getInt("role"));
                    user.setPassword(result.getString("password"));
                    user.setDepartment(result.getString("department"));
                    user.setEmail(result.getString("email"));

                    return user;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static void addUser(User user) {
        //salvestame ettevõtte andmebaasi
        if (userDoesntExsist(user.getEmail())) {
            String sql = "INSERT INTO user (first_name, last_name, password, role, department, email)" +
                    " VALUES ('" + user.getFirstName() + "' , '" + user.getLastName() + "' , '" + user.getPassword() + "' , " + user.getPermissionId() + " , '" + user.getDepartment() + "' , '" + user.getEmail() + "')";
            executeSql(sql);

        }
    }

    public static void modifyUser(User user) {
        String sql = String.format("UPDATE user SET first_name = '%s', last_name = '%s', password = '%s', role = %s, department = '%s', email = '%s' WHERE id = %s",
                user.getFirstName(), user.getLastName(), user.getPassword(), user.getPermissionId(), user.getDepartment(), user.getEmail(), user.getId());
        executeSql(sql);
    }




    public static User getUser(int userId) {
        try {
            String sql = "Select * from user where id =" + userId;
            ResultSet result = executeSql(sql);
            if (result != null) {
                if (result.next()) {
                    User user = new User();
                    user.setId(result.getInt("id"));
                    user.setFirstName(result.getString("first_name"));
                    user.setLastName(result.getString("last_name"));
                    user.setPermissionId(result.getInt("role"));
                    user.setPassword(result.getString("password"));
                    user.setDepartment(result.getString("department"));
                    user.setEmail(result.getString("email"));
                    return user;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static void deleteUser(int userId) {
        String sql = "DELETE FROM user where id = " + userId;
        executeSql(sql);
    }

    /*FEEDBACK under this comment there is all for feedback*/

    public static void addFeedBack(Feedbackform feedback) {
        String sql = "INSERT INTO feedback (id, feedback_points, feedback_comments, meeting_id, )" +
                " VALUES ('" + feedback.getFeedBackFormId() + "' , '" + feedback.getFeedBackAsNumber() + "' , '" + feedback.getComment() + "' , " + feedback.getMeetingId() + "')";
        executeSql(sql);

    }

    public static void submitFeedBack(Feedbackform feedbackform) {
        String sql = "INSERT INTO feedback_form (feedback_points, feedback_comments, meeting_id)" +
                " VALUES (" + feedbackform.getFeedBackAsNumber() + " , '" + feedbackform.getComment() + "' , " + feedbackform.getMeetingId() + ")";
        executeSql(sql);
    }

    /*MEETING under this comment there is all for meetings*/

    public static Meeting getMeetingById(int meetingId) {
        try {
            String sql = "Select * from meeting where id =" + meetingId;
            ResultSet result = executeSql(sql);
            if (result != null) {
                if (result.next()) {
                    Meeting meeting = new Meeting();
                    meeting.setMeetingId(result.getInt("id"));
                    meeting.setMeetingOwnerId(result.getInt("meeting_owner_id"));
                    meeting.setMeetingTypeId(result.getInt("meeting_type_id"));
                    meeting.setDateTimeForMeeting(result.getString("datetime"));
                    meeting.setDetailsOfMeeting(result.getString("details_of_meeting"));
                    meeting.setSubject(result.getString("subject"));
                    meeting.setMeetingHolder(getUser(meeting.getMeetingOwnerId()));
                    return meeting;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }


    public static List<Meeting>  getMeetings(int meetingOwnerId) {
        List<Meeting> meetings = new ArrayList<>();
        try {
            ResultSet result = executeSql("select * from meeting where meeting_owner_id =" + meetingOwnerId);
            if (result != null) {
                while (result.next()) {
                    meetings.add(new Meeting(result));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return meetings;
    }


    public static void addMeeting(Meeting meeting) {
        String sql = "INSERT INTO meeting (meeting_owner_id, meeting_uuid, subject, details_of_meeting, datetime, meeting_type_id)" +
                " VALUES (" + meeting.getMeetingOwnerId() + " , '" + meeting.getUniqueHash() + "' , '" + meeting.getSubject() + "' , '" + meeting.getDetailsOfMeeting() + "' , '" + meeting.getDateTimeForMeeting() + "' , " + meeting.getMeetingTypeId() + ")";
        executeSql(sql);
    }


    public static void modifyMeeting(Meeting meeting) {
        String sql = String.format("UPDATE meeting SET meeting_owner_id = %s, subject = '%s', details_of_meeting = '%s', datetime = '%s', meeting_type_id = %s WHERE id = meeting_uuid",
                meeting.getMeetingOwnerId(), meeting.getSubject(), meeting.getDetailsOfMeeting(), meeting.getDateTimeForMeeting(), meeting.getMeetingTypeId(), meeting.getUniqueHash());
                executeSql(sql);
    }


}
